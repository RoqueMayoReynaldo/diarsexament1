﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProyectoT1.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ProyectoT1.DB;

namespace ProyectoT1.Controllers
{
    public class HomeController : Controller
    {
        private AppProyectT1Context context;
        public HomeController(AppProyectT1Context context)
        {
            this.context = context;
        }
        public IActionResult Index()
        {
            List<BlogPost> blogposts = context.BlogPosts.ToList();
            List<BlogPost> blogp = blogposts.OrderByDescending(x => x.fechac).ToList();

            return View(blogp);
        }

        
    }
}
