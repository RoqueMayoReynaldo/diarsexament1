﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProyectoT1.Models;
using ProyectoT1.DB;

namespace ProyectoT1.Controllers
{
    public class comentBlog {
       public BlogPost blogPost { get; set; }
       public List<Comentario> lcomentario { get; set; }

    }
    public class BlogController : Controller
    {
        private AppProyectT1Context context;
        public BlogController(AppProyectT1Context context)
        {
            this.context = context;
        }
        public ViewResult Registrar()
        {

            return View();
        }
        [HttpGet,HttpPost]
        public String RegistrarBlog(BlogPost blogpost)
        {
            context.BlogPosts.Add(blogpost);
            context.SaveChanges();
            return "Registro realizado";
        }
        public ViewResult Detalle(int id)
        {

            List<Comentario> coments = context.Comentarios.Where(z=>z.idBlog==id).ToList();
            List<Comentario> com = coments.OrderByDescending(x => x.fechac).ToList();
            comentBlog cm = new comentBlog
            {
                blogPost = context.BlogPosts.FirstOrDefault(x => x.id == id),
                lcomentario = com
            };
            return View(cm);
        }
        public RedirectToActionResult addComentario(Comentario comentario)
        {
            context.Comentarios.Add(comentario);
            context.SaveChanges();

            var ob = new { id = comentario.idBlog };
            return RedirectToAction("Detalle",ob);
        }

    }
}
