﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoT1.Models
{
    public class Comentario
    {
        public int id { get; set; }
        public int idBlog { get; set; }
        public string fechac { get; set; }
        public string detalle { get; set; }
    }
}
