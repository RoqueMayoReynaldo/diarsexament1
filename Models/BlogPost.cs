﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoT1.Models
{
    public class BlogPost
    {
        public int id { get; set; }
        public string titulo { get; set; }
        public string autor { get; set; }
        public string fechac { get; set; }
        public string contenido { get; set; }
    }
}
