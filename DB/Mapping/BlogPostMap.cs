﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProyectoT1.Models;
namespace ProyectoT1.DB.Mapping
{
    public class BlogPostMap : IEntityTypeConfiguration<BlogPost>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<BlogPost> builder)
        {
            builder.ToTable("BlogPost", "dbo");
            builder.HasKey(BlogPost=>BlogPost.id);
        }
    }
}
