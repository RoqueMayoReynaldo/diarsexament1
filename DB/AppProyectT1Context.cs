﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProyectoT1.Models;
using ProyectoT1.DB.Mapping;
namespace ProyectoT1.DB
{
    public class AppProyectT1Context : DbContext
    {
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<Comentario> Comentarios { get; set; }
        public AppProyectT1Context(DbContextOptions<AppProyectT1Context> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new BlogPostMap());
            modelBuilder.ApplyConfiguration(new ComentarioMap());
        }
    }
}
